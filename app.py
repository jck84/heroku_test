from flask import Flask, escape, request, render_template

app = Flask(__name__)


@app.route('/', methods=['GET'])
def get_home():
    return render_template('home.htm')

@app.route('/home', methods=['GET'])
def get_index():
    return render_template('index.htm')

@app.route('/new', methods=['GET'])
def get_new():
    return render_template('new.htm')
#@app.route('/new', methods=['GET'])
#def get_home():
#    return 'new item page'

#@app.route('/new', methods=['POST'])
#def post_home():
#    return 'new added'
